# JERC-coffea-tutorial

Coffea tutorial for deriving Jet energy corrections.

We will be running this tutorial on [coffea-casa](https://coffea-casa.readthedocs.io/en/latest/).

Steps:
1. Open https://coffea-opendata.casa 
2. Click on "Authorised users only"
3. Choose "CERN" in CILogon Identity provider and logon.
4. Sign-in with CERN credientials.
